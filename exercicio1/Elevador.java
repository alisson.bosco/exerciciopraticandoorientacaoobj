public class Elevador {

    private int andarAtual;
    private int totalAndares;
    private int capacidade;
    private int pessoasPresentes;

    public Elevador( int capacidade, int totalAndares ) {
        this.capacidade = capacidade;
        this.totalAndares = totalAndares;
        this.andarAtual = 0;
        this.pessoasPresentes = 0;
    }

    public int getAndarAtual() {
        return andarAtual;
    }

    public void setAndarAtual( int andarAtual ) {
        this.andarAtual = andarAtual;
    }

    public int getTotalAndares() {
        return totalAndares;
    }

    public void setTotalAndares( int totalAndares ) {
        this.totalAndares = totalAndares;
    }

    public int getCapacidade() {
        return capacidade;
    }

    public void setCapacidade( int capacidade ) {
        this.capacidade = capacidade;
    }

    public int getPessoasPresentes() {
        return pessoasPresentes;
    }

    public void setPessoasPresentes( int pessoasPresentes ) {
        this.pessoasPresentes = pessoasPresentes;
    }

    public void inicializa() {
        this.andarAtual = 0;
        this.pessoasPresentes = 0;
    }

    public void entra() {
        if ( pessoasPresentes < capacidade ) {
            pessoasPresentes++;
            System.out.println( "Uma pessoa entrou no elevador." );
        } else {
            System.out.println( "O elevador está cheio. Não é possível entrar mais pessoas." );
        }
    }

    public void sai() {
        if ( pessoasPresentes > 0 ) {
            pessoasPresentes--;
            System.out.println( "Uma pessoa saiu do elevador." );
        } else {
            System.out.println( "Não há pessoas no elevador." );
        }
    }

    public void sobe() {
        if ( andarAtual < totalAndares ) {
            andarAtual++;
            System.out.println( "O elevador subiu para o andar " + andarAtual + "." );
        } else {
            System.out.println( "O elevador já está no último andar." );
        }
    }

    public void desce() {
        if ( andarAtual > 0 ) {
            andarAtual--;
            System.out.println( "O elevador desceu para o andar " + andarAtual + "." );
        } else {
            System.out.println( "O elevador já está no térreo." );
        }
    }
}
