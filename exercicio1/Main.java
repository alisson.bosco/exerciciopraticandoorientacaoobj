public class Main {

    public static void main( String[] args ) {
        Elevador elevador = new Elevador( 3, 5 );
        elevador.inicializa(); 
        elevador.entra();
        elevador.sobe();
        elevador.entra();
        elevador.sobe(); 
        elevador.sai();
        elevador.desce();
    }
}
