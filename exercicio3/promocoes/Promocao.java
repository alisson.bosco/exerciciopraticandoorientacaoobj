package exercicio3.promocoes;

public class Promocao {

    private String nome;
    private double desconto;

    public Promocao( String nome, double desconto ) {
        this.nome = nome;
        this.desconto = desconto;
    }

    public String getNome() {
        return nome;
    }

    public double getDesconto() {
        return desconto;
    }
}
