package exercicio3.programaDeFidelidade;

import exercicio3.cliente.Cliente;

public class PontoFidelidade {

    private Cliente cliente;
    private int pontos;

    public void adicionarPontos( int quantidade ) {
        pontos += quantidade;
    }

    public void utilizarPontos( int quantidade ) {
        if ( pontos >= quantidade ) {
            pontos -= quantidade;
            System.out.println( "Pontos utilizados: " + quantidade );
        } else {
            System.out.println( "Saldo de pontos insuficiente." );
        }
    }

    public Cliente getCliente() {
        return cliente;
    }

    public int getPontos() {
        return pontos;
    }
}
