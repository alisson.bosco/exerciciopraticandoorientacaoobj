package exercicio3.produtos;

public class Produto {

    private String nome;
    private int id;
    private double preco;

    public Produto( String nome, double preco, int id ) {
        this.nome = nome;
        this.preco = preco;
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public double getPreco() {
        return preco;
    }

    public int getId() {
        return id;
    }
}
