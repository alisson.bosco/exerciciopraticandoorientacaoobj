package exercicio3;

import java.util.ArrayList;
import java.util.List;
import exercicio3.cliente.Cliente;
import exercicio3.combos.Combo;
import exercicio3.pedidos.Pedido;
import exercicio3.produtos.Produto;
import exercicio3.promocoes.Promocao;

public class Hamburgueria {

    private List< Cliente > clientes;
    private List< Pedido > pedidos;
    private List< Produto > produtos;
    private List< Promocao > promocoes;
    private List< Combo > combos;
    private List< String > mensagens;
    private int contadorPedidos;

    public Hamburgueria() {
        this.clientes = new ArrayList<>();
        this.pedidos = new ArrayList<>();
        this.produtos = new ArrayList<>();
        this.promocoes = new ArrayList<>();
        this.combos = new ArrayList<>();
        this.mensagens = new ArrayList<>();

    }

    public void montarPedido( Cliente cliente, List< Produto > produtos ) {

        if ( contadorPedidos < 50 ) {
            Pedido pedido = new Pedido( cliente, produtos );
            ++contadorPedidos;
            pedidos.add( pedido );
            System.out.println( "Pedido #" + pedido.getNumeroPedido() + " realizado com sucesso." );
        } else {
            System.out.println( "Limite de pedidos atingido para hoje. Tente novamente amanhã." );
        }
    }

    public void selecionarPagamento( Pedido pedido, String formaPagamento ) {
        pedido.setFormaPagamento( formaPagamento );
        System.out.println( "Forma de pagamento selecionada: " + formaPagamento );
    }

    public void selecionarRetiradaEntrega( Pedido pedido, String opcao ) {
        pedido.setOpcaoRetiradaEntrega( opcao );
        System.out.println( "Opção de " + opcao + " selecionada para o pedido #" + pedido.getNumeroPedido() );
    }

    public void atualizarStatusPedido( Pedido pedido, String novoStatus ) {
        pedido.setStatus( novoStatus );
        System.out.println( "Status do pedido #" + pedido.getNumeroPedido() + " atualizado para: " + novoStatus );
    }

    public void cadastrarCliente( Cliente novoCliente ) {
        clientes.add( novoCliente );
    }

    public Cliente buscarCliente( String email ) {
        for ( Cliente cliente : clientes ) {
            if ( cliente.getEmail().equals( email ) ) {
                return cliente;
            }
        }
        return null;
    }

    public void cadastrarProduto( Produto novoProduto ) {
        produtos.add( novoProduto );
    }

    public void cadastrarPromocao( Promocao novaPromocao ) {
        promocoes.add( novaPromocao );
    }

    public void cadastrarCombo( Combo novoCombo ) {
        combos.add( novoCombo );
    }

    public Pedido buscarPedido( int idPedido ) {
        for ( Pedido pedido : pedidos ) {
            if ( pedido.getId() == idPedido ) {
                return pedido;
            }
        }
        return null;
    }

    public void enviarMensagemClientes( String mensagem ) {
        for ( Cliente cliente : clientes ) { // Supondo que você tenha uma lista de clientes chamada "listaClientes"
            System.out.println( "Enviando mensagem para o cliente " + cliente.getNome() + ":" );
            System.out.println( mensagem );
        }
    }

    public List< Produto > getProdutos() {
        return produtos;
    }

    public Produto buscarProduto( int idProduto ) {
        for ( Produto produto : produtos ) {
            if ( produto.getId() == idProduto ) {
                return produto;
            }
        }
        return null;
    }

    public List< String > getMensagens() {
        return mensagens;
    }

    public List< Promocao > getPromocoes() {
        return promocoes;
    }

    public List< Combo > getCombos() {
        return combos;
    }

}
