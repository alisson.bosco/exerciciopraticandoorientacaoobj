package exercicio3.cliente;

import java.util.ArrayList;
import java.util.List;
import exercicio3.pedidos.Pedido;

public class Cliente {

    private String nome;
    private List< Pedido > pedidos;
    private String email;
    private String telefone;
    private String senha;

    public Cliente( String nome, String email, String senha, String telefone ) {
        this.nome = nome;
        this.email = email;
        this.senha = senha;
        this.telefone = telefone;
        this.pedidos = new ArrayList<>();
    }

    public boolean verificarSenha( String senha ) {
        return this.senha.equals( senha );
    }

    public Pedido buscarPedido( int idPedido ) {
        for ( Pedido pedido : pedidos ) {
            if ( pedido.getId() == idPedido ) {
                return pedido;
            }
        }
        return null;
    }

    public void cancelarPedido( Pedido pedido ) {
        pedidos.remove( pedido );
    }

    public List< Pedido > getPedidos() {
        return pedidos;
    }

    public void setNome( String nome2 ) {
        this.nome = nome2;
    }

    public void setSenha( String senha2 ) {
        this.senha = senha2;
    }

    public void setTelefone( String telefone2 ) {
        this.telefone = telefone2;
    }

    public String getNome() {
        return nome;
    }

    public String getEmail() {
        return email;
    }

    public String getTelefone() {
        return telefone;
    }

    public String getSenha() {
        return senha;
    }

}
