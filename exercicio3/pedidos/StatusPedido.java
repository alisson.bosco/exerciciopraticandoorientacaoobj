package exercicio3.pedidos;

public enum StatusPedido {
    NOVO, EM_ANDAMENTO, CONCLUIDO, CANCELADO, PENDENTE
}
