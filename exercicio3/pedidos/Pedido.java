package exercicio3.pedidos;

import java.util.ArrayList;
import java.util.List;
import exercicio3.cliente.Cliente;
import exercicio3.produtos.Produto;

public class Pedido {

    private static int nextId = 1;
    private int id;
    private Cliente cliente;
    private double valorTotal;
    private StatusPedido status;
    private String formaPagamento;
    private int numeroDoPedido;

    public Cliente getCliente() {
        return cliente;
    }

    private List< Produto > produtos;

    public List< Produto > getProdutos() {
        return produtos;
    }

    public Pedido( Cliente cliente, List< Produto > produtos ) {
        this.id = nextId++;
        this.cliente = cliente;
        this.produtos = new ArrayList<>( produtos );
        this.status = StatusPedido.NOVO;
    }

    public void adicionarProduto( Produto produto ) {
        produtos.add( produto );
    }

    public double calcularValorTotal() {
        double total = 0.0;
        for ( Produto produto : produtos ) {
            total += produto.getPreco();
        }
        return this.valorTotal = total;
    }

    public void setFormaPagamento( String formaPagamento ) {
        this.formaPagamento = formaPagamento;
    }

    public void setStatus( String novoStatus ) {
    }

    public void setOpcaoRetiradaEntrega( String opcao ) {
    }

    public StatusPedido getStatus() {
        return status;
    }

    public int getId() {
        return id;
    }

    public int getNumeroPedido() {
        return numeroDoPedido;
    }

    public double getValorTotal() {
        return valorTotal;
    }

    public void setStatus( StatusPedido pendente ) {
    }

    public void setEntrega( boolean b ) {
    }
}
