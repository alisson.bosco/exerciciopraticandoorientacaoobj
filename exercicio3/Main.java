package exercicio3;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import exercicio3.cliente.Cliente;
import exercicio3.combos.Combo;
import exercicio3.pedidos.Pedido;
import exercicio3.produtos.Produto;
import exercicio3.promocoes.Promocao;

public class Main {

    private static Hamburgueria hamburgueria = new Hamburgueria();
    private static Cliente clienteLogado = null;

    public static void main( String[] args ) {
        Scanner scanner = new Scanner( System.in );

        exibirMenuInicial();

        int opcao;
        do {
            System.out.print( "Escolha uma opção: " );

            opcao = scanner.nextInt();
            scanner.nextLine();

            switch ( opcao ) {
                case 1:
                    cadastrarCliente( scanner );
                    break;
                case 2:
                    realizarLogin( scanner );
                    break;
                case 3:
                    exibirMenuCliente( scanner );
                    break;
                case 4:
                    exibirMenuHamburgueria( scanner );
                    break;
                case 5:
                    System.out.println( "Saindo do sistema..." );
                    break;
                default:
                    System.out.println( "Opção inválida. Tente novamente." );
                    break;
            }
        } while ( opcao != 5 );

        scanner.close();
    }

    private static void exibirMenuInicial() {
        System.out.println( "----- Bem-vindo à Hamburgueria -----" );
        System.out.println( "1. Cadastrar cliente" );
        System.out.println( "2. Fazer login" );
        System.out.println( "3. Acesso do cliente" );
        System.out.println( "4. Acesso da hamburgueria" );
        System.out.println( "5. Sair do sistema" );
    }

    private static void cadastrarCliente( Scanner scanner ) {
        System.out.println( "----- Cadastro de Cliente -----" );
        System.out.print( "Nome: " );
        String nome = scanner.nextLine();
        System.out.print( "Email: " );
        String email = scanner.nextLine();
        System.out.print( "Senha: " );
        String senha = scanner.nextLine();
        System.out.print( "Telefone: " );
        String telefone = scanner.nextLine();

        Cliente novoCliente = new Cliente( nome, email, senha, telefone );
        hamburgueria.cadastrarCliente( novoCliente );
        System.out.println( "Cliente cadastrado com sucesso!" );
    }

    private static void realizarLogin( Scanner scanner ) {
        System.out.println( "----- Login de Cliente -----" );
        System.out.print( "Email: " );
        String email = scanner.nextLine();
        System.out.print( "Senha: " );
        String senha = scanner.nextLine();

        Cliente cliente = hamburgueria.buscarCliente( email );
        if ( cliente != null && cliente.verificarSenha( senha ) ) {
            clienteLogado = cliente;
            System.out.println( "Login realizado com sucesso!" );
        } else {
            System.out.println( "Email ou senha inválidos. Tente novamente." );
        }
    }

    private static void exibirMenuCliente( Scanner scanner ) {
        if ( clienteLogado == null ) {
            System.out.println( "Acesso negado. Faça login primeiro." );
            return;
        }

        System.out.println( "----- Acesso do Cliente -----" );
        System.out.println( "1. Realizar um Pedido" );
        System.out.println( "2. Acompanhar status do pedido" );
        System.out.println( "3. Editar cadastro" );
        System.out.println( "4. Cancelar pedido" );
        System.out.println( "5. Voltar" );

        int opcao;
        do {
            System.out.print( "Escolha uma opção: " );
            opcao = scanner.nextInt();
            scanner.nextLine();

            switch ( opcao ) {
                case 1:
                    fazerPedido( scanner );
                    break;
                case 2:
                    acompanharStatusPedido();
                    break;
                case 3:
                    editarCadastro( scanner );
                    break;
                case 4:
                    cancelarPedido();
                    break;
                case 5:
                    System.out.println( "Voltando ao menu principal..." );
                    break;
                default:
                    System.out.println( "Opção inválida. Tente novamente." );
                    break;
            }
        } while ( opcao != 4 );
    }

    private static void fazerPedido( Scanner scanner ) {
        System.out.println( "Produtos disponíveis:" );
        for ( Produto produto : hamburgueria.getProdutos() ) {
            System.out.println( produto.getId() + ". " + produto.getNome() + " - R$ " + produto.getPreco() );
        }

        System.out.print( "Digite o ID do produto que deseja adicionar ao pedido: " );
        int idProduto = scanner.nextInt();
        Produto produtoSelecionado = hamburgueria.buscarProduto( idProduto );

        if ( produtoSelecionado != null ) {
            List< Produto > produtos = new ArrayList<>();
            Pedido pedido = new Pedido( clienteLogado, produtos );
            pedido.adicionarProduto( produtoSelecionado );
            List< Produto > produtoSelecionados = new ArrayList<>();
            produtoSelecionados.add( produtoSelecionado );
            hamburgueria.montarPedido( clienteLogado, produtoSelecionados );
            Double valorTotal = pedido.calcularValorTotal();
            System.out.println( "O valor total de seu pedido ficou: R$:" + valorTotal );
            System.out.println( "Selecione a forma de pagamento:" );
            System.out.println( "1. Dinheiro" );
            System.out.println( "2. Cartão de crédito" );
            int formaPagamento = scanner.nextInt();
            String metodoDePagamento = String.valueOf( formaPagamento );
            pedido.setFormaPagamento( formaPagamento == 1 ? "Dinheiro" : "Cartão de crédito" );
            hamburgueria.selecionarPagamento( pedido, metodoDePagamento );

            System.out.println( "Selecione a opção de retirada/entrega:" );
            System.out.println( "1. Retirada no local" );
            System.out.println( "2. Entrega em domicílio" );
            int opcaoEntrega = scanner.nextInt();
            pedido.setEntrega( opcaoEntrega == 2 );

            System.out.println( "Pedido realizado com sucesso!" );
        } else {
            System.out.println( "Produto não encontrado. Tente novamente." );
        }
    }

    private static void acompanharStatusPedido() {
        System.out.println( "----- Acompanhamento de Pedido -----" );
        if ( clienteLogado.getPedidos().isEmpty() ) {
            System.out.println( "Você não possui pedidos." );
        } else {
            for ( Pedido pedido : clienteLogado.getPedidos() ) {
                System.out.println( "ID do Pedido: " + pedido.getId() );
                System.out.println( "Status: " + pedido.getStatus() );
                System.out.println( "----" );
            }
        }
    }

    private static void editarCadastro( Scanner scanner ) {
        System.out.println( "----- Edição de Cadastro -----" );
        System.out.print( "Nome: " );
        String nome = scanner.nextLine();
        System.out.print( "Senha: " );
        String senha = scanner.nextLine();
        System.out.print( "Telefone: " );
        String telefone = scanner.nextLine();

        clienteLogado.setNome( nome );
        clienteLogado.setSenha( senha );
        clienteLogado.setTelefone( telefone );

        System.out.println( "Cadastro atualizado com sucesso!" );
    }

    private static void cancelarPedido() {
        System.out.println( "----- Cancelamento de Pedido -----" );
        if ( clienteLogado.getPedidos().isEmpty() ) {
            System.out.println( "Você não possui pedidos para cancelar." );
        } else {
            Scanner scanner = new Scanner( System.in );
            System.out.print( "Informe o ID do pedido a ser cancelado: " );
            int idPedido = scanner.nextInt();
            scanner.nextLine();

            Pedido pedido = clienteLogado.buscarPedido( idPedido );
            if ( pedido != null ) {
                clienteLogado.cancelarPedido( pedido );
                System.out.println( "Pedido cancelado com sucesso!" );
            } else {
                System.out.println( "Pedido não encontrado." );
            }
        }
    }

    private static void exibirMenuHamburgueria( Scanner scanner ) {
        System.out.println( "----- Acesso da Hamburgueria -----" );
        System.out.println( "1. Cadastrar produto" );
        System.out.println( "2. Cadastrar promoção" );
        System.out.println( "3. Cadastrar combo" );
        System.out.println( "4. Atualizar status de pedido" );
        System.out.println( "5. Enviar mensagem aos clientes" );
        System.out.println( "6. Voltar" );

        int opcao;
        do {
            System.out.print( "Escolha uma opção: " );
            opcao = scanner.nextInt();
            scanner.nextLine();

            switch ( opcao ) {
                case 1:
                    cadastrarProduto( scanner );
                    break;
                case 2:
                    cadastrarPromocao( scanner );
                    break;
                case 3:
                    cadastrarCombo( scanner );
                    break;
                case 4:
                    atualizarStatusPedido( scanner );
                    break;
                case 5:
                    enviarMensagemClientes( scanner );
                    break;
                case 6:
                    System.out.println( "Voltando ao menu principal..." );
                    break;
                default:
                    System.out.println( "Opção inválida. Tente novamente." );
                    break;
            }
        } while ( opcao != 6 );
    }

    private static void cadastrarProduto( Scanner scanner ) {
        System.out.println( "----- Cadastro de Produto -----" );
        System.out.print( "Nome: " );
        String nome = scanner.nextLine();
        System.out.print( "Preço: " );
        double preco = scanner.nextDouble();
        scanner.nextLine();

        Produto novoProduto = new Produto( nome, preco, 1 );
        hamburgueria.cadastrarProduto( novoProduto );
        System.out.println( "Produto cadastrado com sucesso!" );
    }

    private static void cadastrarPromocao( Scanner scanner ) {
        System.out.println( "----- Cadastro de Promoção -----" );
        System.out.print( "Nome: " );
        String nome = scanner.nextLine();
        System.out.print( "Desconto (%): " );
        double desconto = scanner.nextDouble();
        scanner.nextLine();

        Promocao novaPromocao = new Promocao( nome, desconto );
        hamburgueria.cadastrarPromocao( novaPromocao );
        System.out.println( "Promoção cadastrada com sucesso!" );
    }

    private static void cadastrarCombo( Scanner scanner ) {
        System.out.println( "----- Cadastro de Combo -----" );
        System.out.print( "Nome: " );
        String nome = scanner.nextLine();

        Combo novoCombo = new Combo( null, nome );

        System.out.print( "Informe a quantidade de produtos no combo: " );
        int quantidadeProdutos = scanner.nextInt();
        scanner.nextLine();

        for ( int i = 1; i <= quantidadeProdutos; i++ ) {
            System.out.println( "Produto " + i );
            System.out.print( "Nome: " );
            String nomeProduto = scanner.nextLine();
            System.out.print( "Preço: " );
            double precoProduto = scanner.nextDouble();
            scanner.nextLine();

            Produto produto = new Produto( nomeProduto, precoProduto, i );
            novoCombo.adicionarProduto( produto );
        }

        hamburgueria.cadastrarCombo( novoCombo );
        System.out.println( "Combo cadastrado com sucesso!" );
    }

    private static void atualizarStatusPedido( Scanner scanner ) {
        System.out.println( "----- Atualização de Status de Pedido -----" );
        System.out.print( "Informe o ID do pedido: " );
        int idPedido = scanner.nextInt();
        scanner.nextLine();

        Pedido pedido = hamburgueria.buscarPedido( idPedido );
        if ( pedido != null ) {
            System.out.println( "Status atual: " + pedido.getStatus() );
            System.out.print( "Novo status: " );
            String novoStatus = scanner.nextLine();
            pedido.setStatus( novoStatus );
            System.out.println( "Status atualizado com sucesso!" );
        } else {
            System.out.println( "Pedido não encontrado." );
        }
    }

    private static void enviarMensagemClientes( Scanner scanner ) {
        System.out.println( "----- Envio de Mensagem aos Clientes -----" );
        System.out.print( "Mensagem: " );

        String mensagem = scanner.nextLine();

        hamburgueria.enviarMensagemClientes( mensagem );
        System.out.println( "Mensagem enviada aos clientes com sucesso!" );
    }
}
