package exercicio3.combos;

import java.util.List;
import exercicio3.produtos.Produto;

public class Combo {

    private String nome;
    private List< Produto > produtos;
    private double precoCombo;

    public Combo( List< Produto > produtos, String nome ) {
        this.produtos = produtos;
        this.nome = nome;

    }

    public double getPrecoCombo() {
        return precoCombo;
    }

    public void adicionarProduto( Produto produtoSelecionado ) {
        produtos.add( produtoSelecionado );
    }

    public String getNome() {
        return nome;
    }

    public List< Produto > getProdutos() {
        return produtos;
    }

}
