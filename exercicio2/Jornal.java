package exercicio2;

import java.util.ArrayList;
import java.util.List;

public class Jornal extends ItemLivraria {

    
    private String dataPublicacao;
    private List< Reportagem > reportagens;

    public Jornal( String titulo, String dataPublicacao, double precoVenda ) {
        super( "Jornal", titulo, precoVenda );
        this.dataPublicacao = dataPublicacao;
        this.reportagens = new ArrayList<>();
    }

    public String getDataPublicacao() {
        return dataPublicacao;
    }

    public void adicionarReportagem( Reportagem reportagem ) {
        reportagens.add( reportagem );
    }

    public List< Reportagem > getReportagens() {
        return reportagens;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append( "Jornal: " ).append( getTitulo() );
        sb.append( "\nData de Publicação: " ).append( dataPublicacao );
        sb.append( "\nPreço de Venda: " ).append( getPrecoVenda() );
        sb.append( "\nReportagens: " );
        for ( Reportagem reportagem : reportagens ) {
            sb.append( "\n- " ).append( reportagem.getTitulo() );
        }
        return sb.toString();
    }
}
