package exercicio2;

public class Livro extends ItemLivraria {

    private String[] autores;
    private String editora;
    private String dataPublicacao;

    public Livro( String titulo, String[] autores, String editora, String dataPublicacao, double precoDaVenda ) {
        super( "Livro", titulo, precoDaVenda );
        this.autores = autores;
        this.editora = editora;
        this.dataPublicacao = dataPublicacao;
    }

    public String[] getAutores() {
        return autores;
    }

    public String getEditora() {
        return editora;
    }

    public String getDataPublicacao() {
        return dataPublicacao;
    }

    @Override
    public String toString() {
        return "Livro: " + getTitulo() + "\nAutores: " + String.join( ", ", autores ) + "\nEditora: " + editora
                + "\nData de Publicação: " + dataPublicacao + "\nPreço de Venda: " + getPrecoVenda();
    }
}
