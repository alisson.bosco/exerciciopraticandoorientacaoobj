package exercicio2;

import java.util.ArrayList;
import java.util.List;

public class Revista extends ItemLivraria {

    private String editora;
    private int numeroEdicao;
    private String dataPublicacao;
    private List< Artigo > artigos;

    public Revista( String titulo, String editora, int numeroEdicao, String dataPublicacao, double precoDeVenda ) {
        super( "Revista", titulo, precoDeVenda );
        this.editora = editora;
        this.numeroEdicao = numeroEdicao;
        this.dataPublicacao = dataPublicacao;
        this.artigos = new ArrayList<>();
    }

    public String getEditora() {
        return editora;
    }

    public int getNumeroEdicao() {
        return numeroEdicao;
    }

    public String getDataPublicacao() {
        return dataPublicacao;
    }

    public void adicionarArtigo( Artigo artigo ) {
        artigos.add( artigo );
    }

    public List< Artigo > getArtigos() {
        return artigos;
    }

    @Override
    public String toString() {
        StringBuilder revista = new StringBuilder();
        revista.append( "Revista: " ).append( getTitulo() );
        revista.append( "\nEditora: " ).append( editora );
        revista.append( "\nNúmero da Edição: " ).append( numeroEdicao );
        revista.append( "\nData de Publicação: " ).append( dataPublicacao );
        revista.append( "\nPreço de Venda: " ).append( getPrecoVenda() );
        revista.append( "\nArtigos: " );
        for ( Artigo artigo : artigos ) {
            revista.append( "\n- " ).append( artigo.getTitulo() );
        }
        return revista.toString();
    }
}
