package exercicio2;

public class Artigo {

    private String titulo;
    private String textoDaPublicacao;
    private String[] autores;

    public Artigo( String titulo, String textoDaPublicacao, String[] autores ) {
        this.titulo = titulo;
        this.textoDaPublicacao = textoDaPublicacao;
        this.autores = autores;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getTextoDaPublicacao() {
        return textoDaPublicacao;
    }

    public String[] getAutores() {
        return autores;
    }
}
