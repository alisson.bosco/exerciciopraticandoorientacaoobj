package exercicio2;

public class Main {

    public static void main( String[] args ) {
        Livraria livraria = new Livraria();

        Livro livro1 = new Livro( "Belas adormecidas", new String[] { "Owen King", "Stephen King" }, "Suma",
                "01/01/2017", 49.99 );
        livraria.incluirItem( livro1 );

        Revista revista1 = new Revista( "Quatro Rodas", "Abril", 3, "01/02/2023", 20.0 );
        revista1.adicionarArtigo( new Artigo( "Volkswagen ID.4 chega em breve ao Brasil...",
                "O Volkswagen ID.4 veio ao Brasil pela primeira vez em 2021 para testar sua popularidade por aqui. No ano passado, pudemos test\u00E1-lo em nossa pista. Em mar\u00E7o deste ano, a VW confirmou que o SUV chegaria...",
                new String[] { "Fernando Pires", "Quatro Rodas" } ) );
        revista1.adicionarArtigo( new Artigo(
                "Novo BMW M5 ter\u00E1 vers\u00E3o perua e poder\u00E1 ter V8 h\u00EDbrido de 748 cv",
                "A BMW j\u00E1 est\u00E1 na fase final de desenvolvimento da nova gera\u00E7\u00E3o do M5. De acordo com a montadora, todas as variantes do modelo j\u00E1 entraram na \u201Cparte pr\u00E1tica de seu processo de desenvolvimento\u201D.",
                new String[] { "Jo\u00E3o Vitor Ferreira" } ) );
        livraria.incluirItem( revista1 );

        Jornal jornal1 = new Jornal( "JornalNh", "01/03/2023", 10.0 );
        jornal1.adicionarReportagem( new Reportagem(
                "Ladr\u00E3o de carros \u00E9 preso e solto pela 12\u00AA vez em Novo Hamburgo",
                "Um conhecido ladr\u00E3o de carros de Novo Hamburgo passa a colecionar mais uma pris\u00E3o em flagrante seguida de liberdade provis\u00F3ria. A 12\u00AA nos \u00FAltimos quatro anos. Contido por populares dentro do autom\u00F3vel de uma v\u00EDtima h\u00E1 dois dias, foi solto na manh\u00E3 desta quinta-feira (29) na audi\u00EAncia de cust\u00F3dia.",
                new String[] { "Silvio Milani" } ) );
        livraria.incluirItem( jornal1 );

        livraria.listarTitulos();
        System.out.println();

        livraria.listarItensAVenda();
        System.out.println();

        double precoLivroEstudante = livraria.calcularPrecoVenda( livro1, "Estudante" );
        double precoRevistaEstudante = livraria.calcularPrecoVenda( revista1, "Estudante" );
        double precoJornalEstudante = livraria.calcularPrecoVenda( jornal1, "Estudante" );
        System.out.println( "Preço de venda do Livro para estudante: " + "R$:" + precoLivroEstudante );
        System.out.println( "Preço de venda da Revista para estudante: "  + "R$:" + precoRevistaEstudante );
        System.out.println( "Preço de venda do Jornal para estudante: "  + "R$:" + precoJornalEstudante );

        double precoLivroClienteNormal = livraria.calcularPrecoVenda( livro1, "Cliente Normal" );
        double precoRevistaClienteNormal = livraria.calcularPrecoVenda( revista1, "Cliente Normal" );
        double precoJornalClienteNormal = livraria.calcularPrecoVenda( jornal1, "Cliente Normal" );
        System.out.println( "Preço de venda do Livro para cliente normal: "  + "R$:" + precoLivroClienteNormal );
        System.out.println( "Preço de venda da Revista para cliente normal: "  + "R$:" + precoRevistaClienteNormal );
        System.out.println( "Preço de venda do Jornal para cliente normal: "  + "R$:" + precoJornalClienteNormal );
    }
}
