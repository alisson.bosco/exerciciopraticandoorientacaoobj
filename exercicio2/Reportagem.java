package exercicio2;

public class Reportagem {

    private String titulo;
    private String textoDaReportagem;
    private String[] autores;

    public Reportagem( String titulo, String textoDaReportagem, String[] autores ) {
        this.titulo = titulo;
        this.textoDaReportagem = textoDaReportagem;
        this.autores = autores;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getTextoDaReportagem() {
        return textoDaReportagem;
    }

    public String[] getAutores() {
        return autores;
    }
}
