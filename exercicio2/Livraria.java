package exercicio2;

import java.util.ArrayList;
import java.util.List;

public class Livraria {

    private List< ItemLivraria > itens;

    public Livraria() {
        this.itens = new ArrayList<>();
    }

    public void incluirItem( ItemLivraria item ) {
        itens.add( item );
    }

    public void alterarItem( int codigo, ItemLivraria novoItem ) {
        for ( int i = 0; i < itens.size(); i++ ) {
            if ( itens.get( i ).getCodigo() == codigo ) {
                itens.set( i, novoItem );
                return;
            }
        }
        System.out.println( "Item não foi encontrado." );
    }

    public void listarTitulos() {
        System.out.println( "Listagem dos itens da livraria: código + tipo de item + título" );
        for ( ItemLivraria item : itens ) {
            System.out.println( item.getCodigo() + " - " + item.getTipo() + " - " + item.getTitulo() );
        }
    }

    public void listarItensAVenda() {
        System.out.println( "Listagem dos itens da livraria para venda: todas as informações de cada um dos nosso itens" );
        for ( ItemLivraria item : itens ) {
            System.out.println( item );
        }
    }

    public double calcularPrecoVenda( ItemLivraria item, String modalidade ) {
        double preco = item.getPrecoVenda();
        if ( modalidade.equals( "Estudante" ) && ( item instanceof Livro || item instanceof Revista ) ) {
            preco = preco * 0.5;
        }
        return preco;
    }
}
