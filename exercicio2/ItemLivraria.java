package exercicio2;

public abstract class ItemLivraria {

    private static int proximoCodigo = 1;
    private int codigo;
    private String tipo;
    private String titulo;
    private double precoVenda;

    public ItemLivraria( String tipo, String titulo, double precoVenda ) {
        this.codigo = proximoCodigo++;
        this.tipo = tipo;
        this.titulo = titulo;
        this.precoVenda = precoVenda;
    }

    public int getCodigo() {
        return codigo;
    }

    public String getTipo() {
        return tipo;
    }

    public String getTitulo() {
        return titulo;
    }

    public double getPrecoVenda() {
        return precoVenda;
    }

    public abstract String toString();
}
